package com.example.rodrigokirschner.calculadorarodrigo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class  MainActivity extends AppCompatActivity implements View.OnClickListener{
    float entrada, resultado;//variáveis que armazenam os dados capturados do teclado numérico
    TextView principal, auxiliar;
    String operador, telaAux;
    boolean telaOcupada, semResultado;
    @Override
    protected void onCreate(Bundle savedInstanceState)   {
        System.out.println("Passou onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        principal=(TextView)findViewById(R.id.txtPrimario);
        auxiliar=(TextView)findViewById(R.id.txtSecundario);
        redefinir();
    }

   public void soma(float numero){
       System.out.println("Passou soma");
        resultado= resultado+numero;
    }//método que efetua a soma*/
    public void subtracao(float numero){
        System.out.println("Passou subtracao");
        resultado= resultado-numero;
    }//método que efetua a subtração
    public void multiplicacao(float numero){
        System.out.println("Passou multiplicacao");
        resultado=resultado*numero;
    }//método que efetua a multiplicação
    public void divisao(float numero){
        System.out.print("Passou  número="+numero);
        System.out.println("Passou passou divisao");
        if(numero==0){
            telaOcupada=false;
        }
        else{
            semResultado=false;
            resultado= resultado/numero;
        }
        //mostraResultado();
    }//método que efetua a divisão


    public float numeroTela(String sinal){
            entrada=Float.parseFloat(principal.getText().toString());
            operador=sinal;
        return entrada;
    }

    public float igualDiretoClicado(){
        float telaZerada=numeroTela("");
        return telaZerada;
    }


    public void verificador(String sinal){//método que serve para analizar quais teclas foram clicadas para fazer as operações
        //na ordem certa
        if(sinal.equals("")&&operador.equals("")){
            System.out.println("Passou pela situação em que foi clicado direto no sinal de igual");
            resultado=igualDiretoClicado();//Numero da tela foi armazenado direto pois foi clicado direto no igual
            telaPrincipal(resultado+"");
            telaAuxiliar("");

        }
        else  if(operador.equals("")&& !sinal.equals("")){/*
        Situação em que o operador ainda se encontra vazio mas o sinal ainda não este
        este será o primeiro valor.
        */
            System.out.println("Passou pela situação em que foi inserido o primeiro valor");
            telaAuxiliar(auxiliar.getText().toString()+principal.getText());
            resultado=numeroTela(sinal);
            telaAuxiliar(resultado+sinal);


            }
        else if (!operador.equals("")&&sinal.equals("")){//quer dizer que foi clicado no igual
            telaAuxiliar(auxiliar.getText().toString()+principal.getText());
            igualdade(operador, numeroTela(sinal));
            operador="";


        }
        else if(!operador.equals("")&& sinal.equals("")){

            igualdade(operador, resultado);
        }
        mostraResultado();
    }
    public void concatenador(String qualquer)
    {
        System.out.println("Passou concatenador");
        if(telaOcupada==true){
            telaPrincipal(qualquer);
            telaOcupada=false;
        }
        else if(telaOcupada==false&&semResultado==false)
        {
            telaPrincipal(principal.getText() + qualquer);
        }
    }




    public void igualdade(String sinal, float valor){
        System.out.println("Passou igualdade");
        switch (sinal){
            case "+":
               soma(valor);
                break;
            case "-":
                subtracao(valor);
                break;
            case "/":
                semResultado=true;
                divisao(valor);
                break;
            case "x":
                multiplicacao(valor);
                break;
                default:return;
        }

        operador=sinal;
        mostraResultado();
    }

    public void mostraResultado(){
        System.out.println("Passou mostraResultado");
        if(semResultado==false){
            telaPrincipal(resultado+"");
            telaOcupada=true;
        }
        else if(semResultado==true)
        {
            Toast.makeText(this, "Está sem resultado", Toast.LENGTH_SHORT).show();
            telaPrincipal("Resultado Impossível");
            telaOcupada=true;
        }
    }

    public void ponto(){
        if (!principal.getText().toString().contains(".")){
            concatenador(".");
        }
    }

    public void redefinir(){
        System.out.println("Passou redefinir");
            telaPrincipal("0");
            telaAuxiliar("");
            operador ="";
            semResultado=false;
            telaOcupada=true;
            entrada=0;
            resultado=0;
    }

    @Override
    public void onClick(View v) {
        System.out.println("Passou onClick");
        switch (v.getId()){
            case R.id.btSoma:
                verificador("+");
                break;
            case R.id.btSub:
                verificador("-");
                break;
            case R.id.btDiv:
                verificador("/");
                break;
            case R.id.btMulti:
                verificador("x");
                break;
            case R.id.Zero:
                concatenador("0");
                break;
            case R.id.Um:
                concatenador("1");
                break;
            case R.id.Dois:
                concatenador("2");
                break;
            case R.id.Tres:
                concatenador("3");
                break;
            case R.id.Quatro:
                concatenador("4");
                break;
            case R.id.Cinco:
                concatenador("5");
                break;
            case R.id.Seis:
                concatenador("6");
                break;
            case R.id.Sete:
                concatenador("7");
                break;
            case R.id.Oito:
                concatenador("8");
                break;
            case R.id.Nove:
                concatenador("9");
                break;
            case R.id.ponto:
                ponto();
                break;
            case R.id.redefinir:
                redefinir();
                break;
            case R.id.btIgual:
             verificador("");
              break;

        }

    }
    public void telaAuxiliar(String valorEntrada){/*
    Método que evita a repetição método abaixo:-)
    */
        auxiliar.setText(valorEntrada);
    }
    public void telaPrincipal(String valorEntrada){
        System.out.println("Passou telaPrincipal");
        /*Este método serve para melhor controlar o que está sendo exibido na tela
        * principal evitando a reescritura do trecho abaixo*/
        principal.setText(valorEntrada);
    }
}
